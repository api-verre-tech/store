# —— Inspired by ———————————————————————————————————————————————————————————————
# http://fabien.potencier.org/symfony4-best-practices.html
# https://speakerdeck.com/mykiwi/outils-pour-ameliorer-la-vie-des-developpeurs-symfony?slide=47
# https://blog.theodo.fr/2018/05/why-you-need-a-makefile-on-your-project/
# https://www.strangebuzz.com/en/snippets/the-perfect-makefile-for-symfony
# Setup ————————————————————————————————————————————————————————————————————————

include .env

# Parameters
SHELL         = bash
MS_NAME		  = store

# Executables
EXEC_PHP      = php
COMPOSER      = composer
REDIS         = redis-cli
GIT           = git
YARN          = yarn

# Executables: vendors
# PHPUNIT       = ./vendor/bin/phpunit

# Executables: local only
SYMFONY_BIN   = symfony
DOCKER        = docker
DOCKER_COMP   = docker-compose
DOCKER_EXEC	  = $(DOCKER) exec -i $(MS_NAME)_php.1.$(shell $(DOCKER) service ps -f 'name=$(MS_NAME)_php.1' -f 'desired-state=running' $(MS_NAME)_php -q --no-trunc)
SYMFONY       = $(DOCKER_EXEC) $(EXEC_PHP) bin/console

# Executables: prod only
# CERTBOT       = certbot

# Misc
.DEFAULT_GOAL = help

# —— 🐝 The Strangebuzz Symfony Makefile 🐝 ———————————————————————————————————
help: # Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
	
# —— Composer 🧙‍♂️ ————————————————————————————————————————————————————————————
install: composer.lock # Install vendors according to the current composer.lock file
	$(COMPOSER) install --no-progress --prefer-dist --optimize-autoloader

# —— Symfony 🎵 ———————————————————————————————————————————————————————————————
assets: purge # Install the assets with symlinks in the public folder
	$(SYMFONY) assets:install public/ --symlink --relative

# —— Symfony binary 💻 ————————————————————————————————————————————————————————
cert-install: # Install the local HTTPS certificates
	$(SYMFONY_BIN) server:ca:install

# —— Docker 🐳 ————————————————————————————————————————————————————————————————
stack-deploy: # Deploy the stack of this microservice
	$(DOCKER_COMP) -f docker-compose.yml -f docker-compose.prod.yml config | $(DOCKER) stack deploy -c - $(MS_NAME)

stack-deploy-dev: # Deploy the stack of this microservice
	$(DOCKER) pull registry.gitlab.com/api-verre-tech/$(MS_NAME)/php:latest
	$(DOCKER) pull registry.gitlab.com/api-verre-tech/$(MS_NAME)/nginx:latest
	$(DOCKER_COMP) -f docker-compose.yml -f docker-compose.override.yml config | $(DOCKER) stack deploy -c - $(MS_NAME)

stack-rm: # Remove the stack of this microservice
	$(DOCKER) stack rm $(MS_NAME)

wait-for-mysql: exec-wait-mysql-right # Wait for MySQL to be ready
	bin/wait-for-mysql.sh $(MS_NAME)

CONTAINER_CMD=$(DOCKER) exec -u root -it $(MS_NAME)_php.1.$(shell $(DOCKER) service ps -f 'name=$(MS_NAME)_php.1' -f 'desired-state=running' $(MS_NAME)_php -q --no-trunc)
shell:
	$(CONTAINER_CMD) bash

clear-cache-php:
	$(CONTAINER_CMD) $(EXEC_PHP) bin/console cache:clear
	$(EXEC_PHP) bin/console cache:clear

## —— Projet 🐝 ———————————————————————————————————————————————————————————————
start: stack-deploy wait-for-mysql load-fixtures ## Deploie la stack en production

dev: stack-deploy-dev wait-for-mysql load-fixtures ## Deploie la stack pour le développement et charge les fixtures

stop: stack-rm ## Supprime la stack de ce microservice

test: test-unit-all ## Permet de lancer les tests unitaires et fonctionnels

reload: load-fixtures ## Recharge les fixtures

cc-php: clear-cache-php ## Nettoie le cache du conteneur PHP

load-fixtures: # Crée la BDD, contrôle la validité du schema, charge les fixtures et vérifie les status de migrations
	$(SYMFONY) doctrine:cache:clear-metadata
	$(SYMFONY) doctrine:database:create --if-not-exists
	$(SYMFONY) doctrine:schema:drop --force
	$(SYMFONY) doctrine:schema:create
	$(SYMFONY) doctrine:schema:validate
	$(SYMFONY) doctrine:fixtures:load --no-interaction

# —— Tests ✅ —————————————————————————————————————————————————————————————————
test: phpunit.xml # Run main functional and unit tests
	$(eval testsuite ?= 'main') # or "external"
	$(eval filter ?= '.')
	$(PHPUNIT) --testsuite=$(testsuite) --filter=$(filter) --stop-on-failure

test-all: phpunit.xml # Run all tests
	chmod +x $(PHPUNIT)
	sed -i 's/\r//' $(PHPUNIT)
	$(PHPUNIT) --stop-on-failure

# —— Deploy & Prod 🚀 —————————————————————————————————————————————————————————

env-check: # Check the main ENV variables of the project
	printenv

le-renew: # Renew Let's Encrypt HTTPS certificates
	$(CERTBOT) --apache -d strangebuzz.com -d www.strangebuzz.com

# —— JWT 🕸 ———————————————————————————————————————————————————————————————————

jwt-generate-keys: # Generates the main JWT ket set
	mkdir -p config/jwt
	openssl genpkey -out ./config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
	openssl pkey -in ./config/jwt/private.pem -out ./config/jwt/public.pem -pubout

## —— Bin scripts right 🎓 —————————————————————————————————————————————————————————
exec-wait-mysql-right: ## Give right access for script to wait for mysql
	chmod +x bin/wait-for-mysql.sh
	sed -i 's/\r//' bin/wait-for-mysql.sh