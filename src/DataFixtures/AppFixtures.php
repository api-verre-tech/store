<?php

namespace App\DataFixtures;

use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Stock;
use App\Entity\Store;
use App\Utils\CallAPI;
use Symfony\Component\HttpClient\HttpClient;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // REIMS
        $cityReims = new City();
        $cityReims->setName("Reims");
        $cityReims->setPostalCode("51100");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Reims&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $cityReims->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $cityReims->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($cityReims);

        // NANCY
        $cityNancy = new City();
        $cityNancy->setName("Nancy");
        $cityNancy->setPostalCode("54100");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Nancy&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $cityNancy->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $cityNancy->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($cityNancy);

        // Lille
        $city = new City();
        $city->setName("Lille");
        $city->setPostalCode("59800");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Lille&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Paris
        $city = new City();
        $city->setName("Paris");
        $city->setPostalCode("75001");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Paris&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Marseille
        $city = new City();
        $city->setName("Marseille");
        $city->setPostalCode("13001");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Marseille&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Toulouse
        $city = new City();
        $city->setName("Toulouse");
        $city->setPostalCode("31500");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Toulouse&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Strasbourg
        $city = new City();
        $city->setName("Strasbourg");
        $city->setPostalCode("67200");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Strasbourg&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Metz
        $city = new City();
        $city->setName("Metz");
        $city->setPostalCode("57000");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Metz&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Perpignan
        $city = new City();
        $city->setName("Perpignan");
        $city->setPostalCode("66000");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Perpignan&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Le Havre
        $city = new City();
        $city->setName("Le Havre");
        $city->setPostalCode("76610");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Le%20havre&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        // Caen
        $city = new City();
        $city->setName("Caen");
        $city->setPostalCode("14000");

        $client = HttpClient::create();
        $response = $client->request(
            "GET",
            "https://api-adresse.data.gouv.fr/search/?q=Caen&limit=1"
        );
        $cityInfo = $response->toArray()['features'];

        $city->setLongitude($cityInfo[0]['geometry']['coordinates'][0]);
        $city->setLatitude($cityInfo[0]['geometry']['coordinates'][1]);
        $manager->persist($city);

        $store = new Store();
        $store->setAddress("23 allée du bois");
        $store->setName("Entrepôt Nancy 3");
        $store->setCity($cityNancy);
        $manager->persist($store);

        $stock = new Stock();
        $stock->setFkidM($store);
        $stock->addProduct(1);
        $stock->addProduct(2);
        $stock->addProduct(2);
        $stock->addProduct(2);
        $stock->addProduct(2);
        $stock->addProduct(2);
        $stock->addProduct(2);
        $stock->addProduct(2);
        $manager->persist($stock);


        $store = new Store();
        $store->setAddress("73 rue charlemoine");
        $store->setName("Entrepôt Reims 1");
        $store->setCity($cityReims);
        $manager->persist($store);

        $stock = new Stock();
        $stock->setFkidM($store);
        $stock->addProduct(5);
        $stock->addProduct(5);
        $stock->addProduct(5);
        $stock->addProduct(6);
        $stock->addProduct(6);
        $stock->addProduct(6);
        $stock->addProduct(6);
        $stock->addProduct(7);
        $stock->addProduct(7);
        $stock->addProduct(7);
        $stock->addProduct(7);
        $manager->persist($stock);
        /***********************************/

        $manager->flush();
    }
}
