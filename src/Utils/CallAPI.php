<?php

namespace App\Utils;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Contracts\HttpClient\ResponseInterface;

class CallAPI
{
    public static function fetch(String $method, String $service, String $url, String $token): ResponseInterface
    {
        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../../.env');

        $client = HttpClient::create();
        $servicePort = $_ENV[strtoupper($service) . "_PORT"];
        $response = $client->request(
            $method,
            'http://host.docker.internal:' . $servicePort . '/api' . $url,
            [
                'headers' => !is_null($token)?['Authorization' => $token]:[]
            ]
        );
        return $response;
    }
}
