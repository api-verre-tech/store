<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\StockRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *     collectionOperations={"post", "get"},
 *     itemOperations={
 *         "get",
 *         "get_stock"={
 *             "method"="get",
 *             "path"="/store/inventory/{fkid_m}",
 *             "openapi_context"={
 *                 "summary"="List of all products in a store",
 *                 "requestBody"={
 *                     "content"={
 *                         "application/json"={
 *                              "schema"={
 *                                  "type"="object",
 *                                  "required"={"fkid_m"},
 *                                  "properties"={
 *                                      "fkid_m"={"type"="integer","example"="1"}
 *                                  }
 *                              }
 *                         },
 *                     }
 *                 }
 *             }
 *         }
 *     }
 * )
 * @ORM\Entity(repositoryClass=StockRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"fkid_m.city.id": "end"})
 */
class Stock
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Store::class, inversedBy="stock")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fkid_m;

    /**
     * @ORM\Column(type="array")
     */
    private $products = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFkidM(): ?Store
    {
        return $this->fkid_m;
    }

    public function setFkidM(?Store $fkid_m): self
    {
        $this->fkid_m = $fkid_m;

        return $this;
    }

    public function getProducts(): ?array
    {
        return $this->products;
    }

    public function setProducts(array $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function addProduct(int $product): self
    {
        array_push($this->products, $product);
        
        return $this;
    }
}
